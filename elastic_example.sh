#!/bin/sh

# Logstash opetions
IMAGE_LOGSTASH='docker.elastic.co/logstash/logstash:7.4.2'
NAME_LOGSTASH_LOGS='logstash_logs'
NAME_LOGSTASH_MAIL='logstash_mail'
CONFIGFILE=$(pwd)'/logstash/logstash.yml'
LOGSDIR=$(pwd)'/logs/'
PIPELINELOGS=$(pwd)'/logstash/logstash_logs.conf'
PIPELINEMAIL=$(pwd)'/logstash/logstash_email.conf'

#ELasticsearch options
NAME_ES='es_logs'
VARIABLES_ES='discovery.type=single-node'
IMAGE_ES='docker.elastic.co/elasticsearch/elasticsearch:7.4.2'
#general options
OPTIONS=" -i --network host --detach"

# stop all running containers
sudo docker stop $(sudo docker ps -q)

# run logstash logs container
sudo docker run $OPTIONS --name $NAME_LOGSTASH_LOGS \
	--mount type=bind,source=$CONFIGFILE,target=/usr/share/logstash/config/logstash.yml \
	--mount type=bind,source=$PIPELINELOGS,target=/usr/share/logstash/pipeline/logstash.conf \
	--mount type=bind,source=$LOGSDIR,target=/tmp/logs \
	$IMAGE_LOGSTASH \

# run logstash mail container
sudo docker run $OPTIONS --name $NAME_LOGSTASH_MAIL \
	--mount type=bind,source=$CONFIGFILE,target=/usr/share/logstash/config/logstash.yml \
	--mount type=bind,source=$PIPELINEMAIL,target=/usr/share/logstash/pipeline/logstash.conf \
	$IMAGE_LOGSTASH \

# run elasticsearch container	
sudo docker run $OPTIONS --name $NAME_ES  -e $VARIABLES_ES $IMAGE_ES

#run kibana conatainer
sudo docker run -it --network host --mount type=bind,source=$(pwd)/kibana.yml,target=/usr/share/kibana/config/kibana.yml  docker.elastic.co/kibana/kibana:7.4.2
